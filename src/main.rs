use chat_manager::captcha::repository::CaptchaRepository;
use chat_manager::{captcha, commands, handlers};
use dptree::{endpoint, filter_map_async};
use std::sync::Arc;
use teloxide::filter_command;
use teloxide::prelude::*;
use typesafe_repository::inmemory::InMemoryRepository;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let token = dotenv::var("TELOXIDE_TOKEN").unwrap();
    let bot = Bot::new(token);
    let id = bot
        .get_me()
        .send()
        .await
        .expect("Error getting bot id")
        .user
        .id;
    let handler = dptree::entry()
        .branch(Update::filter_chat_member().endpoint(handlers::chat_member_updated))
        .branch(
            Update::filter_message()
                .branch(
                    filter_map_async(captcha::has_captcha)
                        .branch(
                            filter_command::<captcha::CaptchaCommand, _>()
                                .endpoint(captcha::captcha_command),
                        )
                        .branch(endpoint(captcha::check_captcha)),
                )
                .branch(
                    filter_command::<commands::AdminCommand, _>().endpoint(handlers::admin_command),
                ),
        );
    let captcha_repository: Arc<dyn CaptchaRepository> = Arc::new(InMemoryRepository::new());
    let mut container = DependencyMap::new();
    container.insert(id);
    container.insert(captcha_repository);
    Dispatcher::builder(bot, handler)
        .dependencies(container)
        .enable_ctrlc_handler()
        .default_handler(|upd| async move {
            log::debug!("Unhandled update: {upd:?}");
        })
        .build()
        .dispatch()
        .await;
}
