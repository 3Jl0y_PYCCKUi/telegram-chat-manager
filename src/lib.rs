pub mod commands;
pub mod handlers;
pub mod captcha;

use teloxide::types::User;

pub fn format_user(user: &User) -> String {
    user.username.clone().unwrap_or_else(|| {
        user.last_name
            .clone()
            .map(|l| format!("{} {l}", user.first_name))
            .unwrap_or_else(|| user.first_name.clone())
    })
}
