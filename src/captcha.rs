pub mod repository;

use crate::format_user;
use anyhow::Error;
use captcha::{filters::Noise, Captcha};
use repository::CaptchaRepository;
use std::path::PathBuf;
use std::sync::Arc;
use std::{env, fs};
use teloxide::prelude::*;
use teloxide::types::{ChatId, InputFile, MessageId, User};
use teloxide::utils::command::BotCommands;
use typesafe_repository::{GetIdentity, Identity, RefIdentity};
use uuid::Uuid;

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "lowercase")]
pub enum CaptchaCommand {
    Regenerate,
}

const MAX_REGENERATED: usize = 3;
const MAX_RETRIES: usize = 3;

#[derive(Clone, Debug)]
pub struct UserCaptcha {
    pub answer: String,
    pub message_ids: Vec<MessageId>,
    pub retries: usize,
    pub regenerated: usize,
    pub id: CaptchaId,
}

impl UserCaptcha {
    pub fn new(
        answer: String,
        chat: ChatId,
        user: UserId,
        message_ids: Vec<MessageId>,
        retries: usize,
        regenerated: usize,
    ) -> Self {
        Self {
            answer,
            message_ids,
            retries,
            regenerated,
            id: CaptchaId(chat, user),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct CaptchaId(pub ChatId, pub UserId);

impl Identity for UserCaptcha {
    type Id = CaptchaId;
}

impl GetIdentity for UserCaptcha {
    fn id(&self) -> Self::Id {
        self.id.clone()
    }
}

impl RefIdentity for UserCaptcha {
    fn id_ref(&self) -> &Self::Id {
        &self.id
    }
}

pub async fn send_captcha(id: ChatId, user: &User, bot: &Bot) -> Result<UserCaptcha, Error> {
    let (path, answer) = gen_captcha()?;
    let captcha = InputFile::file(&path);
    let msg = bot
        .send_photo(id, captcha)
        .caption(format!(
            "Hello {}, please confirm that you are human",
            format_user(user)
        ))
        .await?;
    fs::remove_file(path)?;
    Ok(UserCaptcha::new(answer, id, user.id, vec![msg.id], 0, 0))
}

pub async fn check_captcha(
    msg: Message,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    mut captcha: UserCaptcha,
) -> Result<(), Error> {
    match msg.from().zip(msg.text()) {
        Some((user, text)) if text.trim() == captcha.answer => {
            bot.send_message(
                msg.chat.id,
                format!("{} have passed captcha", format_user(user)),
            )
            .await?;
            for message_id in &captcha.message_ids {
                bot.delete_message(msg.chat.id, *message_id).await?;
            }
            bot.delete_message(msg.chat.id, msg.id).await?;
            repo.remove(captcha.id_ref()).await?;
        }
        Some((user, _)) => {
            if captcha.retries == MAX_RETRIES {
                bot.ban_chat_member(msg.chat.id, user.id).await?;
            }
            bot.send_message(
                msg.chat.id,
                format!("Try again\nRetry {} of {}", captcha.retries, MAX_RETRIES),
            )
            .reply_to_message_id(msg.id)
            .await?;
            captcha.retries += 1;
            repo.update(captcha).await?;
        }
        _ => {
            bot.delete_message(msg.chat.id, msg.id).await?;
            captcha.retries += 1;
            repo.update(captcha).await?;
        }
    }
    Ok(())
}

pub async fn captcha_command(
    cmd: CaptchaCommand,
    mut captcha: UserCaptcha,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    msg: Message,
) -> Result<(), Error> {
    if let Some(user) = msg.from() {
        match cmd {
            CaptchaCommand::Regenerate if captcha.regenerated < MAX_REGENERATED => {
                regenerate_captcha(&captcha, repo.clone(), bot, msg.chat.id, user).await?;
                captcha.message_ids.push(msg.id);
                repo.update(captcha).await?;
            }
            CaptchaCommand::Regenerate => {
                bot.send_message(msg.chat.id, "You cannot regenerate captcha anymore")
                    .reply_to_message_id(msg.id)
                    .await?;
                captcha.message_ids.push(msg.id);
                repo.update(captcha).await?;
            }
        }
    }
    Ok(())
}

pub async fn regenerate_captcha(
    captcha: &UserCaptcha,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    id: ChatId,
    user: &User,
) -> Result<(), Error> {
    let mut captcha = captcha.clone();
    let (path, answer) = gen_captcha()?;
    captcha.answer = answer;
    captcha.regenerated += 1;
    let c = InputFile::file(&path);
    let msg = bot
        .send_photo(id, c)
        .caption(format!(
            "{}, please confirm that you are human\nCaptcha regenerated {} of {} times",
            format_user(user),
            captcha.regenerated,
            MAX_REGENERATED,
        ))
        .await?;
    captcha.message_ids.push(msg.id);
    repo.update(captcha).await?;
    Ok(())
}

pub async fn has_captcha(msg: Message, repo: Arc<dyn CaptchaRepository>) -> Option<UserCaptcha> {
    if let Some(user) = msg.from() {
        repo.get_one(&CaptchaId(msg.chat.id, user.id))
            .await
            .unwrap()
    } else {
        None
    }
}

fn gen_captcha() -> Result<(PathBuf, String), Error> {
    let dir = env::temp_dir();
    let mut binding = Uuid::encode_buffer();
    let id = Uuid::new_v4().simple().encode_lower(&mut binding);
    let captcha_path = dir.join(format!("{}.{}", id, "png"));
    let mut binding = Captcha::new();
    let captcha = binding
        .add_chars(5)
        .apply_filter(Noise::new(0.6))
        .view(220, 120);
    let msg = captcha.chars();
    captcha.save(captcha_path.as_path())?;
    Ok((captcha_path, msg.iter().collect::<String>()))
}
