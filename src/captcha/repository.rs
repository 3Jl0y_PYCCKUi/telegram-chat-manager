use super::UserCaptcha;
use anyhow::Error;
use typesafe_repository::prelude::*;

pub trait CaptchaRepository:
    Repository<UserCaptcha, Error = Error>
    + Add<UserCaptcha>
    + Get<UserCaptcha>
    + Update<UserCaptcha>
    + Remove<UserCaptcha>
    + Send
    + Sync
{
}

impl<T> CaptchaRepository for T where
    T: Repository<UserCaptcha, Error = Error>
        + Add<UserCaptcha>
        + Get<UserCaptcha>
        + Update<UserCaptcha>
        + Remove<UserCaptcha>
        + Send
        + Sync
{
}
