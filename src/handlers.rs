use crate::captcha::{self, repository::CaptchaRepository};
use crate::commands::AdminCommand;
use crate::format_user;
use anyhow::anyhow;
use anyhow::Error;
use chrono::prelude::*;
use chrono::Duration;
use regex::Regex;
use teloxide::prelude::*;
use teloxide::types::{ChatMemberKind, ChatMemberUpdated, ChatPermissions, User, UserId};
use std::sync::Arc;
use tokio::time::sleep;
use typesafe_repository::GetIdentity;

fn parse_duration(duration: String) -> Result<i64, Error> {
    fn minutes(x: i64) -> i64 {
        60 * x
    }
    fn hours(x: i64) -> i64 {
        minutes(60) * x
    }
    fn days(x: i64) -> i64 {
        hours(24) * x
    }
    fn weeks(x: i64) -> i64 {
        days(7) * x
    }
    fn years(x: i64) -> i64 {
        days(365) * x
    }

    if !duration.is_ascii() {
        return Err(anyhow!("Not ASCII"));
    };
    let re = Regex::new(r"[smhdwy]").unwrap();
    let duration = duration.to_lowercase();
    let res = re.find(&duration);
    match res {
        None => {
            let parsed_duration = duration.parse()?;
            Ok(parsed_duration)
        }
        Some(..) => {
            let mut res_dur = 0;
            let mut buf = String::from("");
            for char in duration.chars() {
                match char {
                    'y' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += years(parsed_buf);
                    }
                    'w' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += weeks(parsed_buf);
                    }
                    'd' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += days(parsed_buf);
                    }
                    'h' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += hours(parsed_buf);
                    }
                    'm' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += minutes(parsed_buf);
                    }
                    's' => {
                        let parsed_buf: i64 = buf.parse()?;
                        res_dur += parsed_buf;
                    }
                    _ => {
                        let parsed_num = char.to_digit(10);
                        match parsed_num {
                            Some(..) => {
                                buf.push(char);
                            }
                            None => {
                                return Err(anyhow!("Wrong time format"));
                            }
                        };
                    }
                }
            }
            Ok(res_dur)
        }
    }
}

pub async fn mute_user(
    user: &User,
    duration: String,
    msg: &Message,
    bot: Bot,
) -> Result<(), Error> {
    let duration = parse_duration(duration);
    let chat = &msg.chat;
    match duration {
        Err(..) => {
            bot.send_message(chat.id, "Wrong time format")
                .reply_to_message_id(msg.id)
                .await?;
            Ok(())
        }
        Ok(duration) => {
            if duration < 30 && duration != 0 {
                bot.send_message(chat.id,"Time to mute can be 0 (forever) or greater than 30s. 1 Year and more = forever").reply_to_message_id(msg.id).await?;
                return Ok(());
            };
            let mut prem = bot.get_chat(chat.id).await?.permissions().unwrap();
            prem.set(ChatPermissions::SEND_MESSAGES, false);
            let now: DateTime<Utc> = Utc::now();
            let until_date = now + Duration::seconds(duration);
            bot.restrict_chat_member(chat.id, user.id, prem)
                .until_date(until_date)
                .await?;
            Ok(())
        }
    }
}

pub async fn chat_member_updated(
    bot_id: UserId,
    upd: ChatMemberUpdated,
    bot: Bot,
    repo: Arc<dyn CaptchaRepository>,
) -> Result<(), Error> {
    let user = upd.new_chat_member.user;
    if upd.from.id == bot_id {
        return Ok(());
    }
    #[cfg(not(debug))]
    if user.is_bot {
        return Ok(());
    }
    let name = format_user(&user);
    match (upd.old_chat_member.kind, upd.new_chat_member.kind) {
        (
            ChatMemberKind::Owner(_) | ChatMemberKind::Administrator(_) | ChatMemberKind::Member,
            ChatMemberKind::Left,
        ) => {
            let msg = format!("Goodbye {name}");
            bot.send_message(upd.chat.id, msg).await?;
        }
        (ChatMemberKind::Left, ChatMemberKind::Member) => {
            let captcha = captcha::send_captcha(upd.chat.id, &user, &bot).await?;
            let id = captcha.id();
            repo.add(captcha).await?;
            tokio::spawn(async move {
                sleep(tokio::time::Duration::from_secs(30)).await;
                if repo.get_one(&id).await.unwrap().is_some() {
                    bot.send_message(upd.chat.id, format!("{name} have not passed captcha"))
                        .await.unwrap();
                    bot.ban_chat_member(upd.chat.id, user.id).await.unwrap();
                    #[cfg(debug)]
                    bot.unban_chat_member(upd.chat.id, user.id).await.unwrap();
                    repo.remove(&id).await.unwrap();
                }
            });
        }
        _ => (),
    };
    Ok(())
}

pub async fn is_admin(id: ChatId, user: UserId, bot: &Bot) -> Result<bool, Error> {
    let admins = bot.get_chat_administrators(id).await?;
    Ok(admins.iter().any(|m| m.user.id == user))
}

pub async fn from_admin(msg: &Message, bot: &Bot) -> Result<bool, Error> {
    match msg.from() {
        Some(x) if is_admin(msg.chat.id, x.id, bot).await? => Ok(true),
        _ => Ok(false),
    }
}

pub async fn admin_command(msg: Message, cmd: AdminCommand, bot: Bot) -> Result<(), Error> {
    if !from_admin(&msg, &bot).await? {
        return Ok(());
    }
    if let Some(user) = msg.reply_to_message().and_then(Message::from) {
        match cmd {
            AdminCommand::Ban => {
                bot.ban_chat_member(msg.chat.id, user.id).await?;
                #[cfg(debug)]
                bot.unban_chat_member(upd.chat.id, user.id).await?;
                bot.send_message(msg.chat.id, format!("Banned {}", format_user(user)))
                    .await?;
            }
            AdminCommand::Kick => {
                bot.kick_chat_member(msg.chat.id, user.id).await?;
                bot.send_message(msg.chat.id, format!("Kicked {}", format_user(user)))
                    .await?;
            }
            AdminCommand::Mute(duration) => mute_user(user, duration, &msg, bot).await?,
            AdminCommand::BanID(user_id) => {
                let id = UserId(user_id);
                let user = bot.get_chat_member(msg.chat.id, id).await?.user;
                bot.ban_chat_member(msg.chat.id, id).await?;
                bot.send_message(msg.chat.id, format!("Banned {}", format_user(&user)))
                    .await?;
            }
            AdminCommand::KickID(user_id) => {
                let id = UserId(user_id);
                let user = bot.get_chat_member(msg.chat.id, id).await?.user;
                bot.kick_chat_member(msg.chat.id, id).await?;
                bot.send_message(msg.chat.id, format!("Kicked {}", format_user(&user)))
                    .await?;
            }
        }
    } else {
        match cmd {
            AdminCommand::BanID(user_id) => {
                let id = UserId(user_id);
                let user = bot.get_chat_member(msg.chat.id, id).await?.user;
                bot.ban_chat_member(msg.chat.id, id).await?;
                bot.send_message(msg.chat.id, format!("Banned {}", format_user(&user)))
                    .await?;
            }
            AdminCommand::KickID(user_id) => {
                let id = UserId(user_id);
                let user = bot.get_chat_member(msg.chat.id, id).await?.user;
                bot.kick_chat_member(msg.chat.id, id).await?;
                bot.send_message(msg.chat.id, format!("Kicked {}", format_user(&user)))
                    .await?;
            }
            _ => {
                bot.send_message(
                    msg.chat.id,
                    "You must reply to message to perform admin command",
                )
                .reply_to_message_id(msg.id)
                .await?;
            }
        }
    }
    Ok(())
}
