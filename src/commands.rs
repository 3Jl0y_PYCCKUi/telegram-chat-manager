use teloxide::utils::command::BotCommands;

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "lowercase")]
pub enum AdminCommand {
    Ban,
    BanID(u64),
    Kick,
    KickID(u64),
    Mute(String),
}
